const express = require('express')
const app = express()

//informando pro express qual engine para html
app.set('view engine', 'ejs')
app.set('views', './app/views')

//require para um modulo, ou importacao de funcao e etc
//const msg = require('./mod_teste')

module.exports = app