//console.log('Criando um site de noticias')

const http = require('http');

const server = http.createServer( function(req,res){
    var categoria = req.url;

    if(categoria == '/tecnologia'){
        res.end("<h1>Noticias sobre tecnologia</h1>");
    }else if (categoria == '/moda'){
        res.end("<h1>Noticias sobre moda</h1>");
    }else if (categoria == '/beleza'){
        res.end("<h1>Noticias sobre beleza</h1>");
    }else{
        res.end("<h1>Bem vindo ao portal</h1>");
    }
});

server.listen(3001);