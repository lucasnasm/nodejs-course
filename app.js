const app = require('./config/server')

const rotaNoticia = require('./app/routes/noticias')(app)

const rotaHome = require('./app/routes/home')(app)

const rotaFormInc = require('./app/routes/form_inc_not')(app)

//ouvindo na porta e escrevendo mensagem no console(shell)
app.listen(3000, function(){
    console.log('Servidor ON')
})